/* The Fancy Directory Listings */
/* Feb. 2016 - Sunsoo */

$(window).load(function(){

	// directory
	$("td a[href$='/']").addClass("directory");

	// parent directory
	var parentLinkVar = $("td a:contains('Parent Directory')");
	$(parentLinkVar).removeClass("directory").addClass("parent_directory");
	$(parentLinkVar).clone().appendTo("#parentLink");

	// markup file
	$("td a[href$='.html']").addClass("markup_file");
	$("td a[href$='.php']").addClass("markup_file");

	// image file
	$("td a[href$='.jpg']").addClass("image_file");
	$("td a[href$='.png']").addClass("image_file");
	$("td a[href$='.gif']").addClass("image_file");

	// icon
	$("<i class='fa fa-folder-o'></i>").prependTo(".directory");
	$("<i class='fa fa-level-up'></i>").prependTo("td .parent_directory");
	$("<i class='fa fa-arrow-circle-left'></i>").prependTo("#parentLink a");
	$("<i class='fa fa-picture-o'></i>").prependTo(".image_file");

	// qr code
	$(".markup_file").parent().addClass("include_qrcode");
	$("<span class='qrcode_icon'><i class='fa fa-qrcode'></i></span>").prependTo(".include_qrcode");
	$(".qrcode_icon").click(function(){
		$(".dimmed").show();
		$(".layer").hide();
		$("#qrCodeLayer").show();
		var locationHost = $(location).attr("host");
		var locationPathname = $(location).attr("pathname");
		var thisHref = $(this).parent().find(".markup_file").attr("href");
		var qrCodeAddress = "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data="+locationHost+locationPathname+thisHref;
		$(".qrcode_area .inner").css("background-image","url("+qrCodeAddress+")");
		$("#qrCodeLayer .file_name").html("");
		$("<span>"+thisHref+"</span>").prependTo("#qrCodeLayer .file_name");
	});

	// my list
	$("#myList").click(function(){
		$(".dimmed").show();
		$(".layer").hide();
		$("#myListLayer").show();
	});

	$("#highlightExec").click(function(){
		var fileNameList = $("#fileNameList").val().split("\n");
		$("td a").removeClass("highlight");
		for(var i=0;i<fileNameList.length;i++){
			var fileNameListVar = "td a[href*='"+fileNameList[i]+"']";
			$(fileNameListVar).addClass("highlight");
		}
	});

	var fileNameList = ["bingo"];
	$("td a").removeClass("highlight");
	for(var i=0;i<fileNameList.length;i++){
		var fileNameListVar = "td a[href*='"+fileNameList[i]+"']";
		$(fileNameListVar).addClass("highlight");
	}

	$("#highlightRemove").click(function(){
		$("td a").removeClass("highlight");
	});

	// layer close
	$(".close").click(function(){
		$(".dimmed").hide();
		$(this).parent().hide();
	});

});